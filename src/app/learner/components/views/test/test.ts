import { Component, OnInit } from "@angular/core"
import { View } from "../../../../common/class/view"
import { LearnerService } from "../../../services/learner";
import { user } from "../../../../webapp/components/interfaces/user"
import { AuthService } from "../../../../webapp/services/auth"
import { MatSnackBar } from "@angular/material"
import { Router } from "@angular/router"
import { log } from "util";


@Component({
	templateUrl: './test.html',
	styleUrls: [ './test.scss' ]
})


export class TestComponent extends View implements OnInit {

	test: any = null
	user: any = null

	constructor (
		private $authSrv: AuthService,
		private $learnerSrv: LearnerService,
		private $snackBar: MatSnackBar,
		private $router: Router
	){
		super()
	}


	ngOnInit(): void {
		this.isLoading = true

		this.user = this.$authSrv.getToken()

		this.$learnerSrv.startTest( this.user.learner.id )
			.subscribe(
				res => {
					this.isLoading = false
					this.test = this.parserTest(res)
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}

	parserTest( test: any ): any {
		test.blocks.map(block => {
			block.preguntas = block.preguntas.map(question => {
				question.json = JSON.parse(question.json)
				return question
			})
			return block
		})

		return test
	}


	continueTest (): void  {
		this.isLoading = true

		this.$learnerSrv.nextLevelTest( this.user.learner.id, this.test )
			.subscribe(
				res => {
					this.isLoading = false

					if ( !res.blocks ) {
						this.user.learner.calificacion = res.calificacion
						this.user.learner.calificacion_final = res.calificacion_final
						this.$authSrv.setToken(this.user)

						this.$snackBar.open( res.msg, null, { duration: 3000 })

						setTimeout(() => {
							this.$router.navigate(['/learner'])
						}, 2000);
					}
					else {
						document.scrollingElement.scrollTop = 0;
						this.test = this.parserTest(res)
					}

				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	finishTest (): void  {
		this.isLoading = true

		this.$learnerSrv.finishTest( this.user.learner.id, this.test )
			.subscribe(
				res => {
					this.isLoading = false
					this.user.learner.calificacion = res.calificacion
					this.user.learner.calificacion_final = res.calificacion_final
					this.$authSrv.setToken(this.user)
					this.$router.navigate(['/learner'])
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


}