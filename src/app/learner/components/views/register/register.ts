import { Component, OnInit } from "@angular/core"
import { View } from "../../../../common/class/view";
import { AuthService } from "../../../../webapp/services/auth"
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material";
import { LearnerService } from "../../../services/learner";


@Component({
	templateUrl: './register.html',
	styleUrls: [ './register.scss' ]
})


export class RegisterComponent extends View implements OnInit {

	learner: any = {}
	formLearner: FormGroup
	isReadonly: boolean = false


	constructor (
		private $authSrv: AuthService,
		private $learnerSrv: LearnerService,
		private $snackBar: MatSnackBar,
		private $fb: FormBuilder,
	) {
		super()
	}


	ngOnInit (): void {
		this.learner = this.$authSrv.getToken()
		this.isReadonly = !!this.learner.learner
		this.setFormLearner(this.learner.group, this.learner.learner )
	}

	setFormLearner ( group: any, learner: any ): void  {
		this.formLearner = this.$fb.group({
			nombre: [ learner ? learner.nombre : '', [Validators.required]],
			apellido: [ learner ?  learner.apellido : '' , [Validators.required]],
			grupo_id: [ group && group.id ]
		})
	}


	saveFormLearner ({ value }): void  {

		this.isLoading = true

		if( this.learner.learner ){
			this.$learnerSrv.updateLearner(value, this.learner.learner.id)
				.subscribe(
					res => {
						this.isLoading = false
					},
					err => {
						this.isLoading = false
						this.$snackBar.open(err.message, null, { duration: 3000 })
					}
				)
		}
		else {
			this.$learnerSrv.registerLearner(value)
				.subscribe(
					res => {
						this.isLoading = false
						this.learner.learner = res.data.alumno
						this.$authSrv.setToken( this.learner )
					},
					err => {
						this.isLoading = false
						this.$snackBar.open(err.message, null, { duration: 3000 })
					}
				)
		}

	}

}