import { NgModule } from "@angular/core"
import { CommonModule } from '../common/common.config'
import { RouterModule, Routes } from '@angular/router'
import { RegisterComponent } from "./components/views/register/register"
import { TestComponent } from "./components/views/test/test";



const routes: Routes = [
	{ path: 'register', component: RegisterComponent },
	{ path: 'test', component: TestComponent },
	{
		path: '**',
		redirectTo: '/learner/register'
	}
]


@NgModule({
	declarations: [
		RegisterComponent,
		TestComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
	],
	entryComponents: [
	],
	exports: [
		RouterModule
	]
})


export class LearnerRoutingModule {}