import { NgModule } from "@angular/core";
import { LearnerRoutingModule } from './learner.routing'
import { LearnerService } from "./services/learner"


@NgModule({
	imports: [
		LearnerRoutingModule,
	],
	providers: [
		LearnerService
	]
})


export class LearnerModule { }