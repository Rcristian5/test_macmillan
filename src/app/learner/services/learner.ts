import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../environments/environment'
import { Observable } from "rxjs"

@Injectable()


export class LearnerService {

	private apiUrl = environment.api


	constructor(
		private $http: HttpClient
	) { }


	registerLearner(learner: any): Observable<any> {
		return this.$http.post(`${this.apiUrl}/alumno`, learner)
	}


	updateLearner(learner: any, idLearner: number): Observable<any> {
		return this.$http.put(`${this.apiUrl}/alumno/${idLearner}`, learner)
	}


	startTest(idLearner: string | number): Observable<any> {
		return this.$http.post(`${this.apiUrl}/alumno/${idLearner}/examen/start`, {})
	}


	nextLevelTest(idLearner: string | number, test: any): Observable<any> {
		return this.$http.post(`${this.apiUrl}/alumno/${idLearner}/examen/next-level`, test)
	}


	finishTest(idLearner: string | number, test: any): Observable<any> {
		return this.$http.post(`${this.apiUrl}/alumno/${idLearner}/examen/finish`, test)
	}

}