import { Component, OnInit } from "@angular/core"
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { TestService } from "../../../services/test"
import { MatDialogRef, } from '@angular/material'
import { MatSnackBar } from '@angular/material'


@Component({
	templateUrl: './form-test.html',
	styleUrls: [ './form-test.scss' ]
})


export class FormTestComponent implements OnInit {

	formTest: FormGroup
	isLoading: boolean =  false


	constructor (
		private fb: FormBuilder,
		private $testSrv: TestService,
		public dialogRef: MatDialogRef<any>,
		private $snackBar: MatSnackBar
	) {}


	ngOnInit () {
		this.formTest = this.fb.group({
			nombre: ['', Validators.required],
			tipo: ['', Validators.required],
			instrucciones: ['', Validators.required],
			media: ['', Validators.required],
		})
	}

	saveFormTest ({ value }): void {
		this.isLoading = true

		// this.$testSrv.creatTest(value)
		// 	.subscribe(
		// 		res => {
		// 			this.isLoading = false
		// 			this.dialogRef.close()
		// 			this.$snackBar.open("Test create success", null, { duration: 3000 })
		// 		},
		// 		err => {
		// 			this.isLoading = false
		// 			this.$snackBar.open(err.message, null, { duration: 3000 })
		// 		}
		// 	)
	}


}