import { Component, OnInit } from "@angular/core"
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material'
import { FormTestComponent } from "../../dialogs/form-test/form-test"
import { TestService } from "../../../services/test";
import { View } from "../../../../../common/class/view";


@Component({
	templateUrl: './tests.html',
	styleUrls: ['./tests.scss']
})


export class TestsComponent extends View implements OnInit  {

	displayedColumns: string[] = ['name', 'type', 'instruction' ]
	testList: any = []

	constructor(
		private $dialog: MatDialog,
		private $testSrv: TestService,
		private $snackBar: MatSnackBar
	) {
		super ()
	}


	ngOnInit (): void {
		this.isLoading = true
		this.$testSrv.getTests()
			.subscribe(
				res => {
					this.isLoading = false
					this.testList = res.data
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	// openDialogFormTest () {
	// 	const dialogRef = this.$dialog.open(FormTestComponent)
	// 	dialogRef.afterClosed().subscribe(result => {
	// 		this.ngOnInit()
	// 	})
	// }

}