import { NgModule } from "@angular/core"
import { RouterModule, Routes } from '@angular/router'
import { TestsComponent } from "./components/views/tests/tests"
import { CommonModule } from "../../common/common.config"
import { FormTestComponent } from "./components/dialogs/form-test/form-test"
// import { TestComponent } from "./components/views/test/test"



const routes: Routes = [
	{ path: '', component: TestsComponent },
	// { path: ':id-test', component: TestComponent },
]


@NgModule({
	declarations: [
		TestsComponent,
		// TestComponent,
		FormTestComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
	],
	entryComponents: [
		FormTestComponent
	],
	exports: [
		RouterModule
	]
})

export class TestRoutingModule { }