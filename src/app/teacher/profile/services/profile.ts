import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"

@Injectable()


export class ProfileService {

	private apiUrl = environment.api


	constructor(
		private $http: HttpClient
	) { }


	updatProfile( profile: any): Observable<any> {
		return this.$http.put(`${this.apiUrl}/maestro/update`, profile)
	}

	deleteProfile(): Observable<any> {
		return this.$http.delete(`${this.apiUrl}/maestro/delete`,)
	}


	getProfile (): Observable<any> {
		return this.$http.get(`${this.apiUrl}/maestro/mi-info`)
	}


}