import { NgModule } from "@angular/core"
import { ProfileRoutingModule } from "./profile.routing"
import { ProfileService } from "./services/profile"


@NgModule({
	imports: [
		ProfileRoutingModule,
	],
	providers: [
		ProfileService
	]
})


export class ProfileModule { }