import { NgModule } from "@angular/core"
import { RouterModule, Routes } from '@angular/router'
import { CommonModule } from "../../common/common.config"
import { ProfileComponent } from "./components/views/profile/profile"



const routes: Routes = [
	{ path: '', component: ProfileComponent },
]


@NgModule({
	declarations: [
		ProfileComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
	],
	entryComponents: [
	],
	exports: [
		RouterModule
	]
})

export class ProfileRoutingModule { }