import { Component, OnInit } from "@angular/core"
import { FormGroup, FormBuilder, Validators } from "@angular/forms"
import { View } from "../../../../../common/class/view"
import { ProfileService } from "../../../services/profile"
import { MatSnackBar } from "@angular/material"
import { AuthService } from "../../../../../webapp/services/auth";


@Component({
	templateUrl: 'profile.html',
	styleUrls: [ 'profile.scss' ]
})


export class ProfileComponent extends View implements OnInit  {

	profile: any = {}
	formProfile: FormGroup
	isReadonly: boolean = true


	constructor (
		private $profileSrv: ProfileService,
		private $snackBar: MatSnackBar,
		private $fb: FormBuilder,
		private $authSrv: AuthService
	) {
		super ()
	}


	ngOnInit (): void  {
		this.isLoading = true

		this.$profileSrv.getProfile()
			.subscribe(
				res => {
					this.profile = res.data
					this.isLoading = false
					this.setFormProfile()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	setFormProfile (): void  {
		this.formProfile = this.$fb.group({
			nombre: [ this.profile.nombre, [Validators.required] ],
			email: [  this.profile.email, [Validators.required] ],
			password: ['']
		})
	}


	saveFormProfile ({ value }): void {
		this.isLoading = true

		this.$profileSrv.updatProfile(value)
			.subscribe(
				res => {
					this.isLoading = false
					this.isReadonly = true
					this.$snackBar.open("Profile update success", null, { duration: 3000 })
				},
				err => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				},
			)
	}


	deleteProfile (): void {
		this.isLoading = true
		this.$profileSrv.deleteProfile()
			.subscribe(
				res => {
					this.isLoading = false
					this.$authSrv.signout()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}

}