import { NgModule } from "@angular/core"
import { GroupsRoutingModule } from './groups.routing'
import { GroupService } from "./services/group"
import { TestService } from "./services/test";
import { LearnerService } from "./services/learner";



@NgModule({
	imports: [
		GroupsRoutingModule,
	],
	providers: [
		GroupService,
		TestService,
		LearnerService
	]
})


export class GroupsModule {}