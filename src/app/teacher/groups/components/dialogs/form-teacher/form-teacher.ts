import { Component, OnInit, Inject } from "@angular/core"
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { MatSnackBar } from '@angular/material'
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { TeacherService } from "../../../services/teacher"



@Component({
	templateUrl: './form-teacher.html',
	styleUrls: [ './form-teacher.scss' ]
})


export class FormTeacherComponent implements OnInit {

	formTeacher: FormGroup
	isLoading: boolean = false


	constructor (
		private $fb: FormBuilder,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private $teacherSrv: TeacherService,
		public dialogRef: MatDialogRef<any>,
		private $snackBar: MatSnackBar
	) {}


	ngOnInit () {
		this.formTeacher = this.$fb.group({
			nombre: [ '', [Validators.required] ],
			password: [ '', [Validators.required] ],
			email: [ '', [Validators.required, Validators.email] ],
		})
	}

	saveFormTeacher({ value }): void {

		this.isLoading = true
		this.$teacherSrv.createTeacher( this.data.idSchool, value )
			.subscribe(
				res => {
					this.isLoading = false
					this.dialogRef.close()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open( err.message, null, { duration: 3000 })
				}
			)
	}

}