import { Component, OnInit } from '@angular/core'
import { FormGroup, Validators, FormBuilder } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { MatSnackBar } from '@angular/material'
import { teacher } from '../../../interfaces/teacher'
import { View } from '../../../../../common/class/view'
import { TeacherService } from '../../../services/teacher'
import { MatDialog } from '@angular/material'
import { GroupService } from '../../../services/group'
import { LearnerService } from '../../../services/learner';


@Component({
	templateUrl: './group.html',
	styleUrls: [ './group.scss' ]
})


export class GroupComponent extends View implements OnInit {

	group: any
	formGroup: FormGroup
	isReadonly: boolean = true
	displayedColumns: Array<string> = ['name', 'code', 'qualification', 'qualification_finish', 'actions']
	idGroup: string


	constructor(
		private $activatedRoute: ActivatedRoute,
		private $groupSrv: GroupService,
		private $snackBar: MatSnackBar,
		private $fb: FormBuilder,
		private $router: Router,
		private $dialog: MatDialog,
		private $learnerSrv: LearnerService
	) {
		super()
	}


	ngOnInit() {
		this.idGroup = this.$activatedRoute.snapshot.paramMap.get('id-group')
		this.isLoading = true

		this.$groupSrv.getGroup( this.idGroup )
			.subscribe(
				res => {
					this.isLoading = false
					this.group = res.grupo
					this.setFormTeacher()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	setFormTeacher (): void {
		this.formGroup = this.$fb.group({
			nombre: [ this.group.nombre, [Validators.required] ],
			codigo_acceso: [this.group.codigo_acceso, [Validators.required] ],
		})
	}


	saveFormGroup({ value }): void  {

		this.isLoading = true

		delete value.codigo_acceso

		this.$groupSrv.updateGroup( this.idGroup, value )
			.subscribe(
				res => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open("Group update success", null, { duration: 3000 })
				},
				err => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				},
			)
	}


	// deleteGroup (): void {
	// 	this.isLoading = true
	// 	this.$groupSrv.deleteGroup( this.idGroup )
	// 		.subscribe(
	// 			res => {
	// 				this.isLoading = false
	// 				// this.$router.navigate([`/teacher//${this.idTeacher}`])
	// 			},
	// 			err => {
	// 				this.isLoading = false
	// 				this.$snackBar.open(err.message, null, { duration: 3000 })
	// 			}
	// 		)
	// }


	resetScore( learner: any ) {
		this.isLoading = true

		this.$learnerSrv.resetScores( this.idGroup, learner.id)
			.subscribe(
				res => {
					this.isLoading = false
					this.$snackBar.open('Reset is success', null, { duration: 3000 })
					this.ngOnInit()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}

}