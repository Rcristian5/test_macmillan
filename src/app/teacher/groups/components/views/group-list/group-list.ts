import { Component, OnInit } from '@angular/core'
import { MatDialog } from '@angular/material'
import { MatSnackBar } from '@angular/material'
import { teacher } from '../../../interfaces/teacher'
import { GroupService } from '../../../services/group'
import { FormGroupComponent } from '../../dialogs/form-group/form-group'


@Component({
	templateUrl: './group-list.html',
	styleUrls: [ './group-list.scss' ]
})


export class GroupListComponent implements OnInit {

	teacherList: Array<teacher> = []
	isLoading: boolean = false
	displayedColumns: Array<string> = ['nombre', 'code_access', 'actions']


	constructor(
		private $groupSrv: GroupService,
		private $snackBar: MatSnackBar,
		private $dialog: MatDialog,
	) {}


	ngOnInit() {
		this.isLoading = true
		this.$groupSrv.getGroups()
			.subscribe(
				res => {
					this.isLoading = false
					this.teacherList = res.data
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	openCreateTeacherDialog () {
		const dialogRef = this.$dialog.open(FormGroupComponent)
		dialogRef.afterClosed().subscribe(result => {
			this.ngOnInit()
		})
	}

}