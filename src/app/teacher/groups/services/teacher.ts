import { Injectable } from "@angular/core"
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"
import { teacher } from "../interfaces/teacher"

@Injectable()


export class TeacherService {

	private apiUrl = environment.api


	constructor (
		private $http: HttpClient
	) {}


	createTeacher ( idSchool: number|string, teacher: teacher ): Observable<any> {
		return this.$http.post(`${this.apiUrl}/admin/escuelas/${idSchool}/maestros`, teacher )
	}


	deleteTeacher ( idSchool: string | number, idTeacher: string | number ): Observable<any> {
		return this.$http.delete(`${this.apiUrl}/admin/escuelas/${idSchool}/maestros/${idTeacher}` )
	}


	updateTeacher(idSchool: string | number,  idTeacher: string | number, teacher: teacher ): Observable<any> {
		return this.$http.put(`${this.apiUrl}/admin/escuelas/${idSchool}/maestros/${idTeacher}`, teacher )
	}


	getTeacher ( idTeacher: string | number ): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/maestro/${idTeacher}/grupos` )
	}

}