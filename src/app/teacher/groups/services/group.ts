import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"

@Injectable()


export class GroupService {

	private apiUrl = environment.api


	constructor(
		private $http: HttpClient
	) { }


	createGroup( group: any): Observable<any> {
		return this.$http.post(`${this.apiUrl}/maestro/crear-grupo`, group)
	}


	// deleteGroup( idGroup: string | number): Observable<any> {
	// 	return this.$http.delete(`${this.apiUrl}/maestro/grupo/${idGroup}`)
	// }


	updateGroup( idGroup: string | number, group: any): Observable<any> {
		return this.$http.put(`${this.apiUrl}/maestro/updategrupo/${idGroup}`, group)
	}


	getGroup( idGroup: string | number): Observable<any> {
		return this.$http.get(`${this.apiUrl}/maestro/grupo/${idGroup}/alumnos`)
	}

	getGroups(): Observable<any> {
		return this.$http.get(`${this.apiUrl}/maestro/grupos`)
	}

}