import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CommonModule } from '../../common/common.config'

import { GroupListComponent } from './components/views/group-list/group-list'
import { FormGroupComponent } from './components/dialogs/form-group/form-group'
import { GroupComponent } from './components/views/group/group'
import { FormTeacherComponent } from './components/dialogs/form-teacher/form-teacher'
import { TeacherComponent } from './components/views/teacher/teacher';
import { TestComponent } from '../test/components/views/_test/test';



const routes: Routes = [
	{ path: '', component: GroupListComponent },
	{ path: ':id-group', component: GroupComponent },
]


@NgModule({
	declarations: [
		GroupListComponent,
		GroupComponent,
		TeacherComponent,
		TestComponent,
		FormGroupComponent,
		FormTeacherComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild( routes ),
	],
	entryComponents: [
		FormGroupComponent,
		FormTeacherComponent
	],
	exports: [
		RouterModule
	]
})

export class GroupsRoutingModule { }