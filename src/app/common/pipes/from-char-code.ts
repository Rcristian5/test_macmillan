import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'ascci'
})


export class FormCharCode implements PipeTransform {

	transform(value: string, args: string[] ): any {
		if ( !value ) {
			return value;
		}

		return String.fromCharCode( parseInt(value, 10) );
	}
}