import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { CommonModule as CommonsModule } from '@angular/common'
import {
	MatCardModule,
	MatButtonModule,
	MatSelectModule,
	MatInputModule,
	MatRadioModule,
	MatIconModule,
	MatTooltipModule,
	MatCheckboxModule,
	MatSlideToggleModule,
	MatDialogModule,
	MatTableModule,
	MatDividerModule,
	MatToolbarModule,
	MatSnackBarModule,
	MatProgressSpinnerModule,
	MatProgressBarModule,
	MatChipsModule
} from '@angular/material'
import { FormCharCode } from './pipes/from-char-code';


@NgModule({
	declarations: [
		FormCharCode
	],
	imports: [
		FormsModule,
		ReactiveFormsModule,
		CommonsModule,
		MatCardModule,
		MatButtonModule,
		MatSelectModule,
		MatInputModule,
		MatRadioModule,
		MatIconModule,
		MatTooltipModule,
		MatCheckboxModule,
		MatSlideToggleModule,
		MatDialogModule,
		MatTableModule,
		MatDividerModule,
		MatToolbarModule,
		MatSnackBarModule,
		MatProgressSpinnerModule,
		MatProgressBarModule,
		MatChipsModule
	],
	exports: [
		FormsModule,
		ReactiveFormsModule,
		CommonsModule,
		MatCardModule,
		MatButtonModule,
		MatSelectModule,
		MatInputModule,
		MatRadioModule,
		MatIconModule,
		MatTooltipModule,
		MatCheckboxModule,
		MatSlideToggleModule,
		MatDialogModule,
		MatTableModule,
		MatDividerModule,
		MatToolbarModule,
		MatSnackBarModule,
		MatProgressSpinnerModule,
		MatProgressBarModule,
		MatChipsModule,
		FormCharCode
	]
})


export class CommonModule {}