import { NgModule } from "@angular/core"
import { RouterModule, Routes } from '@angular/router'
import { DashboardComponent } from "./components/views/dashboard/dashboard"


const routes: Routes = [
	{ path: '', component: DashboardComponent },
]


@NgModule({
	declarations: [
		DashboardComponent
	],
	imports: [
		RouterModule.forChild(routes),
	],
	exports: [
		RouterModule
	]
})

export class DashboardRoutingModule {}