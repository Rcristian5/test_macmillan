import { NgModule } from "@angular/core";
import { DashboardRoutingModule } from './dashboard.routing'


@NgModule({
	imports: [
		DashboardRoutingModule,
	]
})


export class DashboardModule { }