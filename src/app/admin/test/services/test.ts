import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"

@Injectable()


export class TestService {

	private apiUrl = environment.api


	constructor(
		private $http: HttpClient
	) { }


	creatTest( test: any ): Observable<any> {
		return this.$http.post(`${this.apiUrl}/admin/examenes`, test)
	}


	deletTest(idTest: string | number): Observable<any> {
		return this.$http.delete(`${this.apiUrl}/admin/examenes/${idTest}`)
	}


	updatTest( idTest: string | number, test: any): Observable<any> {
		return this.$http.put(`${this.apiUrl}/admin/examenes/${idTest}`, test)
	}


	getTests (): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/examenes`)
	}

	getTest ( idTest: string | number ): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/examenes/${idTest}`)
	}

}