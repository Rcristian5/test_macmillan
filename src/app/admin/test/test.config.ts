import { NgModule } from "@angular/core"
import { TestRoutingModule } from './test.routing'
import { TestService } from "./services/test"



@NgModule({
	imports: [
		TestRoutingModule,
	],
	providers : [
		TestService
	]
})


export class TestModule { }