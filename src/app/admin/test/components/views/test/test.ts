import { Component, OnInit } from '@angular/core'
import { View } from '../../../../../common/class/view';
import { FormGroup, Validators, FormBuilder } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { TestService } from '../../../services/test';
import { MatSnackBar } from '@angular/material'


@Component({
	templateUrl: './test.html',
	styleUrls: [ './test.scss' ]
})


export class TestComponent extends View implements OnInit {

	test: any
	formTest: FormGroup
	isReadonly: boolean = true
	idTest: string


	constructor(
		private $activatedRoute: ActivatedRoute,
		private $testSrv: TestService,
		private $snackBar: MatSnackBar,
		private $fb: FormBuilder,
		private $router: Router,
	) {
		super ()
	}


	ngOnInit() {
		this.idTest = this.$activatedRoute.snapshot.paramMap.get('id-test')
		this.isLoading = true

		this.$testSrv.getTest( this.idTest )
			.subscribe(
				res => {
					this.isLoading = false
					this.test = res.data
					this.setFormTest()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	setFormTest (): void {
		this.formTest = this.$fb.group({
			nombre: [ this.test.nombre, [Validators.required] ],
			tipo: [ this.test.tipo, [Validators.required] ],
			instrucciones: [ this.test.instrucciones, [Validators.required ] ],
			media: [ this.test.media, [Validators.required] ],
		})
	}


	saveFormTest({ value }): void  {

		this.isLoading = true

		this.$testSrv.updatTest( this.idTest, value)
			.subscribe(
				res => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open("Test update success", null, { duration: 3000 })
				},
				err => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				},
			)
	}


	deleteTest (  ): void {
		this.isLoading = true
		this.$testSrv.deletTest(this.idTest)
			.subscribe(
				res => {
					this.isLoading = false
					this.$router.navigate(['/manager/tests'] )
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}

}