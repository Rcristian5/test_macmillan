import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CommonModule } from '../../common/common.config'
import { SchoolListComponent } from './components/views/school-list/school-list'
import { FormSchoolComponent } from './components/dialogs/form-school/form-school'
import { SchoolComponent } from './components/views/school/school'
import { TeacherComponent } from './components/views/teacher/teacher'
import { FormTeacherComponent } from './components/dialogs/form-teacher/form-teacher'
import { FormGroupComponent } from './components/dialogs/form-group/form-group'
import { GroupComponent } from './components/views/group/group'


const routes: Routes = [
	{ path: '', pathMatch: '', redirectTo: 'school-list' },
	{ path: 'school-list', component: SchoolListComponent },
	{ path: ':id-school', component: SchoolComponent },
	{ path: ':id-school/teacher/:id-teacher', component: TeacherComponent },
	{ path: ':id-school/teacher/:id-teacher/group/:id-group', component: GroupComponent },
]


@NgModule({
	declarations: [
		SchoolListComponent,
		SchoolComponent,
		TeacherComponent,
		GroupComponent,
		FormSchoolComponent,
		FormTeacherComponent,
		FormGroupComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild( routes ),
	],
	entryComponents: [
		FormSchoolComponent,
		FormTeacherComponent,
		FormGroupComponent
	],
	exports: [
		RouterModule
	]
})

export class SchoolRoutingModule { }