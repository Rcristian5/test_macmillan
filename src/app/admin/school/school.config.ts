import { NgModule } from "@angular/core"
import { SchoolRoutingModule } from './school.routing'
import { SchoolService } from "./services/school"
import { TeacherService } from "./services/teacher"
import { TestService } from "./services/test"
import { GroupService } from "./services/group"
import { LearnerService } from "./services/learner"


@NgModule({
	imports: [
		SchoolRoutingModule,
	],
	providers: [
		SchoolService,
		TeacherService,
		TestService,
		GroupService,
		LearnerService
	]
})


export class SchoolModule {}