export interface  school {
    id?: string | number
    nombre: string
    telefono: string
    email: string
    contacto: string
    logo: any
}