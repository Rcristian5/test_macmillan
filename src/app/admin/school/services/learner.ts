import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"

@Injectable()


export class LearnerService {

	private apiUrl = environment.api


	constructor(
		private $http: HttpClient
	) { }


	resetScores( idLearner: string | number ): Observable<any> {
		return this.$http.post(`${this.apiUrl}/admin/alumno/${idLearner}/reset`, {})
	}

}