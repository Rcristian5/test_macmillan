import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"

@Injectable()


export class TestService {

	private apiUrl = environment.api


	constructor(
		private $http: HttpClient
	) { }


	// createSchool(formSchool: FormData): Observable<any> {
	// 	return this.$http.post(`${this.apiUrl}/admin/escuelas`, formSchool)
	// }


	// deleteSchool(id: string | number): Observable<any> {
	// 	return this.$http.delete(`${this.apiUrl}/admin/escuelas/${id}`)
	// }


	// updateSchool(id: string | number, formSchool: FormData): Observable<any> {
	// 	formSchool.append('_method', 'put')
	// 	return this.$http.post(`${this.apiUrl}/admin/escuelas/${id}`, formSchool)
	// }


	getTests(): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/examenes`)
	}


	// getSchool(id: string | number): Observable<any> {
	// 	return this.$http.get(`${this.apiUrl}/admin/escuelas/${id}`)
	// }

}