import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"

@Injectable()


export class GroupService {

	private apiUrl = environment.api


	constructor(
		private $http: HttpClient
	) { }


	createGroup( idTeacher: string, group: any): Observable<any> {
		return this.$http.post(`${this.apiUrl}/admin/maestro/${idTeacher}/grupos`, group)
	}


	deleteGroup(idTeacher: string | number, idGroup: string | number): Observable<any> {
		return this.$http.delete(`${this.apiUrl}/admin/maestro/${idTeacher}/grupos/${idGroup}`)
	}


	updateGroup(idTeacher: string | number, idGroup: string | number, group: any): Observable<any> {
		return this.$http.put(`${this.apiUrl}/admin/maestro/${idTeacher}/grupos/${idGroup}`, group)
	}


	getGroup(idTeacher: string | number, idGroup: string | number): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/maestro/${idTeacher}/grupos/${idGroup}`)
	}

}