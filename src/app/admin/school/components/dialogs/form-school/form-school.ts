import { Component, OnInit } from "@angular/core"
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { MatDialogRef } from '@angular/material'
import { MatSnackBar } from '@angular/material'
import { SchoolService } from "../../../services/school"



@Component({
	templateUrl: './form-school.html',
	styleUrls: [ './form-school.scss' ]
})


export class FormSchoolComponent implements OnInit {

	formSchool: FormGroup
	file: File = null
	isLoading: boolean = false


	constructor (
		private $fb: FormBuilder,
		private $schoolSrv: SchoolService,
		public dialogRef: MatDialogRef<any>,
		private $snackBar: MatSnackBar
	) {}


	ngOnInit () {
		this.formSchool = this.$fb.group({
			nombre: [ '', [Validators.required] ],
			telefono: [ '', [Validators.required] ],
			email: [ '', [Validators.required, Validators.email] ],
			contacto: [ '', [Validators.required] ],
			logo: [ '' ],
		})
	}


	previewImage ( event ){
		this.file = <File> event.target.files[0]
	}

	saveFormSchool({ value }): void {

		let formData = new FormData()

		for( const prop in value ) {
			formData.append( prop, value[prop] )
		}

		if( this.file ) {
			formData.append( 'logo', this.file, this.file.name )
		}

		this.isLoading = true

		this.$schoolSrv.createSchool(formData)
			.subscribe(
				res => {
					this.isLoading = false
					this.dialogRef.close()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open( err.message, null, { duration: 3000 })
				}
			)
	}

}