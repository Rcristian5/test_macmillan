import { Component, OnInit, Inject } from "@angular/core"
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { MatSnackBar } from '@angular/material'
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { TestService } from "../../../services/test";
import { test } from "../../../interfaces/test";
import { GroupService } from "../../../services/group"



@Component({
	templateUrl: './form-group.html',
	styleUrls: [ './form-group.scss' ]
})


export class FormGroupComponent implements OnInit {

	formGroup: FormGroup
	isLoading: boolean = false
	testList: Array<test>


	constructor (
		private $fb: FormBuilder,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private $groupSrv: GroupService,
		public dialogRef: MatDialogRef<any>,
		private $snackBar: MatSnackBar,
		private $testSrv: TestService
	) {}


	ngOnInit () {

		this.isLoading = true
		this.$testSrv.getTests()
			.subscribe(
				res => {
					this.isLoading = false
					this.testList = res.data

				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)

		this.formGroup = this.$fb.group({
			nombre: [ '', [Validators.required] ],
			examens_id: [ '', [Validators.required] ],
		})

	}

	saveFormGroup({ value }): void {

		this.isLoading = true

		this.$groupSrv.createGroup( this.data.idTeacher, value )
			.subscribe(
				res => {
					this.isLoading = false
					this.dialogRef.close()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open( err.message, null, { duration: 3000 })
				}
			)
	}

}