import { Component, OnInit } from '@angular/core'
import { MatSnackBar } from '@angular/material'
import { FormSchoolComponent } from '../../dialogs/form-school/form-school';
import { MatDialog } from '@angular/material'
import { school } from '../../../interfaces/school'
import { SchoolService } from '../../../services/school'


@Component({
    templateUrl: 'school-list.html',
    styleUrls: [ './school-list.scss' ]
})


export class SchoolListComponent implements OnInit {

    schoolList: Array<school> = []
    displayedColumns: string[] = ['id', 'name', 'phone', 'email', 'actions' ]
    isLoading: boolean = false

    constructor (
        private $dialog: MatDialog,
        private $schoolSrv: SchoolService,
        private $snackBar: MatSnackBar
    ) {}


    ngOnInit () {
        this.isLoading = true
        this.$schoolSrv.getSchools()
			.subscribe(
				res => {
                    this.schoolList = res.data
                    this.isLoading = false
                },
				err => {
                    this.$snackBar.open( err.message, null, { duration: 3000 } )
                    this.isLoading = false
                }
			)
    }


    openDialogFormSchool () {
		const dialogRef = this.$dialog.open( FormSchoolComponent )
		dialogRef.afterClosed().subscribe( result => {
			this.ngOnInit()
		})
	}

}