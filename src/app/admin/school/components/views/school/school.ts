import { Component, OnInit } from '@angular/core'
import { school } from '../../../interfaces/school'
import { MatDialog } from '@angular/material'
import { FormGroup, Validators, FormBuilder } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { SchoolService } from '../../../services/school'
import { MatSnackBar } from '@angular/material'
import { teacher } from '../../../interfaces/teacher'
import { FormTeacherComponent } from '../../dialogs/form-teacher/form-teacher'


@Component({
	templateUrl: './school.html',
	styleUrls: [ './school.scss' ]
})


export class SchoolComponent implements OnInit {

	school: school
	teacherList: Array<teacher> = []
	file: File = null
	formSchool: FormGroup
	isReadonly: boolean = true
	isLoading: boolean = false
	displayedColumns: Array<string> = ['nombre', 'email', 'actions']
	idSchool: string


	constructor(
		private $activatedRoute: ActivatedRoute,
		private $schoolSrv: SchoolService,
		private $snackBar: MatSnackBar,
		private $fb: FormBuilder,
		private $router: Router,
		private $dialog: MatDialog,
	) {}


	ngOnInit() {
		this.idSchool = this.$activatedRoute.snapshot.paramMap.get('id-school')
		this.isLoading = true
		this.$schoolSrv.getSchool( this.idSchool )
			.subscribe(
				res => {
					this.isLoading = false
					this.school = res.escuela
					this.teacherList = res.maestros
					this.setFormSchool()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	previewImage(event) {
		this.file = <File>event.target.files[0]
	}


	setFormSchool (): void {
		this.formSchool = this.$fb.group({
			nombre: [ this.school.nombre, [Validators.required] ],
			telefono: [ this.school.telefono, [Validators.required] ],
			email: [ this.school.email, [Validators.required, Validators.email] ],
			contacto: [ this.school.contacto, [Validators.required] ],
			logo: [ this.school.logo ],
		})
	}


	saveFormSchool({ value }): void  {

		let formData = new FormData()

		for (const prop in value) {
			formData.append(prop, value[prop])
		}

		formData.delete('logo')

		if (this.file) {
			formData.append('logo', this.file, this.file.name)
		}

		this.isLoading = true

		this.$schoolSrv.updateSchool( this.school.id, formData)
			.subscribe(
				res => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open("School update success", null, { duration: 3000 })
				},
				err => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				},
			)
	}


	deleteSchool (  ): void {
		this.isLoading = true
		this.$schoolSrv.deleteSchool(this.school.id)
			.subscribe(
				res => {
					this.isLoading = false
					this.$router.navigate(['/manager/school'] )
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}

	openCreateTeacherDialog () {
		const dialogRef = this.$dialog.open(FormTeacherComponent, { data: {idSchool: this.idSchool} })
		dialogRef.afterClosed().subscribe(result => {
			this.ngOnInit()
		})
	}

}