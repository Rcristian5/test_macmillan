import { Component, OnInit } from '@angular/core'
import { FormGroup, Validators, FormBuilder } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { MatSnackBar } from '@angular/material'
import { teacher } from '../../../interfaces/teacher'
import { View } from '../../../../../common/class/view'
import { TeacherService } from '../../../services/teacher'
import { MatDialog } from '@angular/material'
import { FormGroupComponent } from '../../dialogs/form-group/form-group';


@Component({
	templateUrl: './teacher.html',
	styleUrls: [ './teacher.scss' ]
})


export class TeacherComponent extends View implements OnInit {

	teacher: teacher
	formTeacher: FormGroup
	isReadonly: boolean = true
	displayedColumns: Array<string> = ['name', 'code_access', 'actions']
	idTeacher: string
	idSchool: string


	constructor(
		private $activatedRoute: ActivatedRoute,
		private $teacherSrv: TeacherService,
		private $snackBar: MatSnackBar,
		private $fb: FormBuilder,
		private $router: Router,
		private $dialog: MatDialog,
	) {
		super()
	}


	ngOnInit() {
		this.idTeacher = this.$activatedRoute.snapshot.paramMap.get('id-teacher')
		this.idSchool = this.$activatedRoute.snapshot.paramMap.get('id-school')
		this.isLoading = true
		this.$teacherSrv.getTeacher( this.idTeacher )
			.subscribe(
				res => {
					this.isLoading = false
					this.teacher = res.maestro
					this.setFormTeacher()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	setFormTeacher (): void {
		this.formTeacher = this.$fb.group({
			nombre: [ this.teacher.nombre, [Validators.required] ],
			password: [ '' ],
			email: [ this.teacher.email, [Validators.required] ],
		})
	}


	saveFormTeacher({ value }): void  {

		this.isLoading = true

		this.$teacherSrv.updateTeacher( this.idSchool, this.idTeacher, value )
			.subscribe(
				res => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open("Teacher update success", null, { duration: 3000 })
				},
				err => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				},
			)
	}


	deleteTeacher (): void {
		this.isLoading = true
		this.$teacherSrv.deleteTeacher( this.idSchool, this.idTeacher )
			.subscribe(
				res => {
					this.isLoading = false
					this.$router.navigate([`/manager/school/${this.idSchool}`])
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	openDialogFormGroup (): void {
		const dialogRef = this.$dialog.open(FormGroupComponent, {
			data: { idTeacher: this.idTeacher }
		})
		dialogRef.afterClosed().subscribe(result => {
			this.ngOnInit()
		})
	}

}