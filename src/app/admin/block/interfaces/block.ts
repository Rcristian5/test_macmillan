export interface block {
	id?: number
	tipo: string
	nivel: string
	media: number
	instrucciones: string
	task: string
	engine: string
	contentido: string
}
