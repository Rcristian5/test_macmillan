import { NgModule } from "@angular/core";
import { BlockRoutingModule } from './block.routing'
import { BlocksService } from "./services/blocks"
import { QuestionService } from "./services/question"


@NgModule({
	imports: [
		BlockRoutingModule,
	],
	providers: [
		BlocksService,
		QuestionService
	]
})


export class BlockModule {}