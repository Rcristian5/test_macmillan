import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CommonModule } from '../../common/common.config'

import { BlockListComponent } from './components/views/block-list/block-list'
import { FormBlockComponent } from './components/dialogs/form-block/form-block'

import { BlockComponent } from './components/views/block/block'
import { QuestionComponent } from './components/views/question/question'


const routes: Routes = [
	{ path: '', component: BlockListComponent },
	{ path: ':id-block', component: BlockComponent },
	{ path: ':id-block/question/:id-question', component: QuestionComponent },
]

@NgModule({
	declarations: [
		BlockListComponent,
		FormBlockComponent,

		BlockComponent,
		QuestionComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild( routes ),
	],
	entryComponents: [
		FormBlockComponent
	],
	exports: [
		RouterModule,
	]
})

export class BlockRoutingModule { }