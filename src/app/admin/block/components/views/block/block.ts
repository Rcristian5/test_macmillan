import { Component, OnInit } from "@angular/core"
import { ActivatedRoute, Router } from "@angular/router"
import { MatSnackBar } from '@angular/material'
import { BlocksService } from "../../../services/blocks"
import { block } from "../../../interfaces/block"
import { question } from "../../../interfaces/question"
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { QuestionService } from "../../../services/question"
import { View } from "../../../../../common/class/view"


@Component({
	selector: 'view-questions',
	templateUrl: './block.html',
	styleUrls:  ['./block.scss']
})


export class BlockComponent extends View implements OnInit {

	block: block
	file: File = null
	formBlock: FormGroup
	isReadonly: boolean = true
	questionList: Array<question> = []
	displayedColumns: string[] = ['description', 'actions']


	constructor(
		private $activatedRoute: ActivatedRoute,
		private $blockSrv: BlocksService,
		private $snackBar: MatSnackBar,
		private $fb: FormBuilder,
		private $router: Router,
		private $questionSrv: QuestionService
	) {
		super()
	}


	ngOnInit() {
		let id = this.$activatedRoute.snapshot.paramMap.get('id-block')
		this.isLoading = true
		this.$blockSrv.getBlock( id )
			.subscribe(
				res => {
					res.data.media = res.data.media + ''
					this.block = res.data
					this.setFormBlock()
					this.getQuestions()
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}


	previewImage(event) {
		this.file = <File>event.target.files[0]
	}


	setFormBlock (): void {
		this.formBlock = this.$fb.group({
			id: [ this.block.id ],
			engine: [ this.block.engine, [ Validators.required ] ],
			media: [ this.block.media, [ Validators.required ] ],
			contentido: [ this.block.contentido ],
			nivel: [ this.block.nivel, [ Validators.required ] ],
			task: [ this.block.task ],
			tipo: [ this.block.tipo, [ Validators.required ] ],
			instrucciones: [ this.block.instrucciones, [ Validators.required ] ]
		})
	}


	getQuestions (): void {
		this.$questionSrv.getQuestions( this.block.id )
			.subscribe(
				res => {
					this.questionList = (res && res && res.preguntas) || []
					this.isLoading = false
				},
				err => {
					this.$snackBar.open(err.message, null, { duration: 3000 })
					this.isLoading = false
				}
			)
	}


	saveFormBlock({ value }): void  {

		let formData = new FormData()

		for (const prop in value) {
			formData.append(prop, value[prop])
		}
		formData.delete('contentido')

		if (this.file) {
			formData.append('contentido', this.file, this.file.name)
		}

		this.isLoading = true
		this.$blockSrv.updateBlock( value.id, formData)
			.subscribe(
				res => {
					this.isReadonly = true
					this.isLoading = false
				},
				err => {
					this.isReadonly = true
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				},
			)
	}


	deleteBlock (  ): void {
		this.isLoading = true
		this.$blockSrv.deleteBlock(this.block.id)
			.subscribe(
				res => {
					this.isLoading = false
					this.$router.navigate(['/manager/block'] )
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}
}