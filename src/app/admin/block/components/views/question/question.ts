import { Component, OnInit } from "@angular/core"
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from "@angular/router"
import { QuestionService } from "../../../services/question"
import { question } from "../../../interfaces/question"
import { MatSnackBar } from "@angular/material"
import { BlocksService } from "../../../services/blocks"
import { View } from "../../../../../common/class/view"


@Component({
	templateUrl: './question.html',
	styleUrls: [ './question.scss' ]
})


export class QuestionComponent extends View implements OnInit {

	private idBlock: string
	private idQuestion: string
	private engine: string
	isReadonly: boolean = true
	formQuestion: FormGroup


	constructor (
		private fb: FormBuilder,
		private $activatedRoute: ActivatedRoute,
		private $questionSrv: QuestionService,
		private $blockSrv: BlocksService,
		private $router: Router,
		private $snackBar: MatSnackBar
	) {
		super()
	}


	ngOnInit(): void {
		this.idBlock = this.$activatedRoute.snapshot.paramMap.get('id-block')
		this.idQuestion = this.$activatedRoute.snapshot.paramMap.get('id-question')

		if( this.idQuestion === 'new' ) {
			this.setFormEmpty()
		}
		else {
			this.isLoading = true
			this.$questionSrv.getQuestion( this.idBlock, this.idQuestion )
				.subscribe(
					res => {
						this.isLoading = false
						this.engine = res.bloque.engine
						this.setFormQuestion(res.pregunta)
					},
					err => {
						this.isLoading = false
						this.$snackBar.open(err.message, null, { duration: 3000 })
					}
				)
		}
	}


	setFormEmpty (): void {

		this.isLoading = true
		this.$blockSrv.getBlock(this.idBlock)
			.subscribe(
				res => {
					this.isLoading = false
					this.engine = res.data.engine
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)

		this.formQuestion = this.fb.group({
			descripcion: ['', [Validators.required] ],
			json: this.fb.group({
				question: ['', [Validators.required] ],
				correct: [''],
				answers: this.fb.array([
					this.initAnswer()
				])
			})
		})
	}


	setFormQuestion ( question: question ): void {
		try {
			question.json = JSON.parse( question.json )
			this.formQuestion = this.fb.group({
				descripcion: [ question.descripcion, [Validators.required]],
				json: this.fb.group({
					question: [ question.json.question, [Validators.required]],
					correct: [ question.json.correct],
					answers: this.fb.array([])
				})
			})

			question.json.answers.map( item => {
				this.addAnswer(item)
			})
		} catch (error) {
			this.$snackBar.open('Question format is invalid', null, { duration: 3000 })
		}
	}


	initAnswer( answer: { correct: string, answer: string } = { correct: '', answer: '' } ): FormGroup {
		return this.fb.group({
			correct: [ answer.correct ],
			answer: [ answer.answer, [Validators.required] ]
		})
	}


	addAnswer( answer: {correct: string, answer: string} ) {
		const control = <FormArray>this.formQuestion.controls['json']['controls']['answers']
		control.push( this.initAnswer(answer) )
	}


	removeAnswer( i: number) {
		const control = <FormArray>this.formQuestion.controls['json']['controls']['answers']
		control.removeAt(i)
	}


	saveQuestion ({ value }): void {
		const formaterQuestion = {
			json: JSON.stringify(value.json),
			descripcion: value.descripcion
		}

		this.isLoading = true
		if (this.idQuestion === 'new') {
			this.$questionSrv.createQuestion( this.idBlock, formaterQuestion)
				.subscribe(
					res => {
						this.isLoading = false
						this.$router.navigate([`/manager/block/${this.idBlock}`])
					},
					err => {
						this.isLoading = false
						this.$snackBar.open( err.message, null, { duration: 3000 } )
					}
				)
		}
		else {
			this.$questionSrv.updateQuestion(this.idBlock, this.idQuestion, formaterQuestion)
				.subscribe(
					res => {
						this.isLoading = false
						this.$router.navigate([`/manager/block/${this.idBlock}`])
					},
					err => {
						this.isLoading = false
						this.$snackBar.open( err.message, null, { duration: 3000 } )
					}
				)
		}

		this.isReadonly = true
	}


	deleteQuestion(): void {
		this.isLoading = true
		this.$questionSrv.deleteQuestion(this.idBlock, this.idQuestion)
			.subscribe(
				res => {
					this.isLoading = false
					this.$router.navigate([`/manager/block/${this.idBlock}`])
				},
				err => {
					this.isLoading = false
					this.$snackBar.open(err.message, null, { duration: 3000 })
				}
			)
	}

}