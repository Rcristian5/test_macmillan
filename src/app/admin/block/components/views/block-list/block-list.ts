import { Component, OnInit } from '@angular/core'
import { MatSnackBar } from '@angular/material'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { FormBlockComponent } from '../../dialogs/form-block/form-block'
import { BlocksService } from '../../../services/blocks'
import { block } from '../../../interfaces/block'
import { View } from '../../../../../common/class/view'


@Component({
	selector: 'view-blocks',
	templateUrl: './block-list.html',
	styleUrls: ['./block-list.scss' ]
})


export class BlockListComponent extends View implements OnInit {

	displayedColumns: string[] = ['id', 'engine', 'level', 'type', 'media', 'task', 'actions' ]
	blockList: Array<block> = []

	constructor(
		private $dialog: MatDialog,
		private $blocksSrv: BlocksService,
		private $snackBar: MatSnackBar
	) {
		super()
	}


	ngOnInit (): void {
		this.isLoading = true
		this.$blocksSrv.getBlocks()
			.subscribe(
				res => {
					this.isLoading = false
					this.blockList = res.data
				},
				err => {
					this.isLoading = false
					this.$snackBar.open( err.message, null, { duration: 3000 } )
				}
			)
	}


	openDialogFormBlock () {
		const dialogRef = this.$dialog.open( FormBlockComponent )
		dialogRef.afterClosed().subscribe( result => {
			this.ngOnInit()
		})
	}


}