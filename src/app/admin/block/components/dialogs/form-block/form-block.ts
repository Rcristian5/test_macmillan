import { Component, OnInit } from "@angular/core"
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { BlocksService } from "../../../services/blocks"
import { MatDialogRef } from '@angular/material'
import { MatSnackBar } from '@angular/material'
import { View } from "../../../../../common/class/view"



@Component({
	templateUrl: './form-block.html',
	styleUrls: [ './form-block.scss' ]
})


export class FormBlockComponent extends View implements OnInit {

	formBlock: FormGroup
	file: File = null


	constructor (
		private $fb: FormBuilder,
		private $blockSrv: BlocksService,
		public dialogRef: MatDialogRef<any>,
		private $snackBar: MatSnackBar
	) {
		super()
	}


	ngOnInit () {
		this.formBlock = this.$fb.group({
			engine: [ '', [Validators.required] ],
			media: [ '0', [Validators.required] ],
			nivel: [ '', [Validators.required] ],
			task: [ '' ],
			tipo: [ '', [Validators.required] ],
			instrucciones: [ '', [Validators.required] ]
		})
	}


	previewImage ( event ){
		this.file = <File> event.target.files[0]
	}

	saveFormBlock({ value }): void {

		let formData = new FormData()

		for( const prop in value ) {
			formData.append( prop, value[prop] )
		}

		if( value.media !== "0" ) {
			if( this.file ) {
				formData.append( 'contentido', this.file, this.file.name )
			}
			else if ( value.media === "1" ) {
				this.$snackBar.open('Image is required', null, { duration: 3000 })
				return
			}
			else if ( value.media === "2" ) {
				this.$snackBar.open('Audio is required', null, { duration: 3000 })
				return
			}
		}

		this.isLoading = true
		this.formBlock.disable()
		this.$blockSrv.createBlock(formData)
		.subscribe(
			res => {
				this.isLoading = false
				this.dialogRef.close()
			},
			err => {
				this.formBlock.enable()
					this.isLoading = false
					this.$snackBar.open( err.message, null, { duration: 3000 })
				}
			)
	}

}