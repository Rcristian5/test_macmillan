import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"
import { block } from "../interfaces/block";
import { question } from "../interfaces/question";

@Injectable()


export class QuestionService {

	private apiUrl = environment.api


	constructor(
		private $http: HttpClient
	) { }


	createQuestion(idBlock: string | number, question: question): Observable<any> {
		return this.$http.post(`${this.apiUrl}/admin/bloques/${idBlock}/preguntas`, question)
	}


	updateQuestion(idBlock: string | number, idQuestion: string | number, question: question): Observable<any> {
		return this.$http.put(`${this.apiUrl}/admin/bloques/${idBlock}/preguntas/${idQuestion}`, question)
	}


	deleteQuestion(idBlock: string | number, idQuestion: string | number): Observable<any> {
		return this.$http.delete(`${this.apiUrl}/admin/bloques/${idBlock}/preguntas/${idQuestion}`)
	}


	getQuestions( idBlock: string | number ): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/bloques/${idBlock}/preguntas`)
	}


	getQuestion( idBlock: string | number, idQuestion: string | number): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/bloques/${idBlock}/preguntas/${idQuestion}`)
	}

}