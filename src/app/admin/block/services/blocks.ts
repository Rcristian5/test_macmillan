import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from "rxjs"
import { block } from "../interfaces/block";

@Injectable()


export class BlocksService {

	private apiUrl = environment.api


	constructor (
		private $http: HttpClient
	) {}


	createBlock ( formBlock: FormData ): Observable<any> {
		return this.$http.post(`${this.apiUrl}/admin/bloques`, formBlock )
	}


	deleteBlock ( id: string | number ): Observable<any> {
		return this.$http.delete(`${this.apiUrl}/admin/bloques/${id}` )
	}


	updateBlock(id: string | number, formBlock: FormData ): Observable<any> {
		formBlock.append('_method', 'put')
		return this.$http.post(`${this.apiUrl}/admin/bloques/${id}`, formBlock )
	}


	getBlocks (): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/bloques` )
	}


	getBlock ( id: string | number ): Observable<any> {
		return this.$http.get(`${this.apiUrl}/admin/bloques/${id}` )
	}

}