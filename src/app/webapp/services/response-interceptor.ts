import { Injectable } from '@angular/core'
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { tap, catchError } from 'rxjs/operators'



@Injectable()



export class ResponseInterceptor implements HttpInterceptor {

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(request).pipe(
			catchError( (err) => {
				if ( err instanceof HttpErrorResponse ) {
					err = err.error
					if( typeof err.message === 'string' ){
						err.message = err.message
					}
					else if ( typeof err.error === 'string' ) {
						err.message = err.error
					}
					else {
						err.message = 'Ocurrio un error.'
					}
				}
				return throwError(err)
			})
		)
	}
}