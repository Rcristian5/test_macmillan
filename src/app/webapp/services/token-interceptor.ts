import { Injectable } from '@angular/core'
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http'
import { AuthService } from './auth'
import { Observable } from 'rxjs'

@Injectable()


export class TokenInterceptor implements HttpInterceptor {

	constructor(
		private $authSrv: AuthService
	) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		request = request.clone({
			setHeaders: {
				Authorization: `Bearer ${this.$authSrv.getToken() && this.$authSrv.getToken().token}`
			}
		})
		return next.handle(request)
	}
}