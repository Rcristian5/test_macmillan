import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, BehaviorSubject, throwError } from 'rxjs'
import { Router } from '@angular/router'
import { map, catchError } from 'rxjs/operators'
import { environment } from '../../../environments/environment'
import { user } from '../components/interfaces/user';


@Injectable()


export class AuthService {

	private apiUrl: string = environment.api
	private loggedIn = new BehaviorSubject<user>({
		isLogin: false
	})


	constructor (
		private $http: HttpClient,
		private $router: Router
	) {
		const user = this.getToken()
		if( user ){
			this.loggedIn.next(user)
		}
	}


	get isLoggedIn() {
		return this.loggedIn.asObservable()
	}


	signinAdmin( email: string, password: string ): Observable<any> {
		return this.$http.post(
			`${this.apiUrl}/oauth/token`,
			{
				"grant_type": "password",
				"client_id": "4",
				"client_secret": "aR8eREBGJeOslmqHMzLSCktJ5OCnBCMXvbwRTTOe",
				"username": email,
				"password": password
			}
		)
		.pipe(
			map( (res: any) => {
				const user = {
					isLogin: true,
					token: res.access_token,
					role: res.user.admin === "true" ? 'admin' : 'teacher',
					name: res.user.nombre,
					email: res.user.email
				}
				this.setToken( user )
				this.loggedIn.next(user)
				return user
				}
			)
		)

	}


	signinLearner( codeAccess: String ): Observable<any> {
		return this.$http.post(`${this.apiUrl}/examen/login`, { codigo_acceso: codeAccess} )
			.pipe(
				map((res: any) => {
					const user = {
						isLogin: true,
						token: res.data.accessToken,
						group: res.data.grupo,
						learner: res.data.alumno,
						role: 'learner',
					}
					this.setToken(user)
					this.loggedIn.next(user)
					return user
				}
				)
			)
	}


	signout(): void {
		localStorage.clear()
		this.loggedIn.next({
			isLogin: false
		})
		this.$router.navigate( ['/login'] )
	}


	getToken (): user {
		return JSON.parse(localStorage.getItem(environment.tokenName))
	}


	setToken(user: user): void {
		localStorage.setItem( environment.tokenName, JSON.stringify(user))
	}

}



