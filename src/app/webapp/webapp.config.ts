import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { CommonModule } from '../common/common.config'
import { HttpClientModule } from '@angular/common/http'

import { WebappComponent } from './components/webapp/webapp'
import { WebappRoutingModule } from './webapp.routing'
import { HeaderComponent } from './components/header/header'
import { SigninComponent } from './components/signin/signin'

import { AuthService } from './services/auth'
import { TokenInterceptor } from './services/token-interceptor'
import { ResponseInterceptor } from './services/response-interceptor';




@NgModule({
	declarations: [
		HeaderComponent,
		WebappComponent,
		SigninComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		CommonModule,
		WebappRoutingModule,
		HttpClientModule
	],
	providers: [
		AuthService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: TokenInterceptor,
			multi: true
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ResponseInterceptor,
			multi: true
		}
	],
	bootstrap: [
		WebappComponent
	]
})


export class AppModule { }
