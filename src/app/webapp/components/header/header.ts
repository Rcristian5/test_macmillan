import { Component, OnInit } from "@angular/core"
import { AuthService } from "../../services/auth"
import { Observable } from "rxjs"
import { user } from "../interfaces/user"


@Component({
	selector: 'app-header',
	templateUrl: 'header.html',
	styleUrls: [ 'header.scss' ]
})


export class HeaderComponent implements OnInit {

	isLoggedIn: Observable<user>

	constructor(
		private $authSrv: AuthService
	) {}

	ngOnInit() {
		this.isLoggedIn = this.$authSrv.isLoggedIn
	}

	onLogout() {
		this.$authSrv.signout()
	}

}