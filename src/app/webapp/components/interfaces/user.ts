export interface user {
	isLogin: boolean
	token?: string
	role?: string
	email?: string
	name?: string
}