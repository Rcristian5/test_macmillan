import { Component, OnInit } from "@angular/core"
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AuthService } from "../../services/auth"
import { MatSnackBar } from '@angular/material'
import { Router } from "@angular/router"


@Component({
	templateUrl: './signin.html',
	styleUrls: [ './signin.scss' ]
})


export class SigninComponent implements OnInit {
	isAdminSignin: boolean = true
	formAdmin: FormGroup
	formLearner: FormGroup
	innerWidth = 0;


	constructor (
		private $router: Router,
		private $fb: FormBuilder,
		private $authSrv: AuthService,
		private $snackBar: MatSnackBar
	) {}


	ngOnInit (): void  {

		this.formAdmin = this.$fb.group({
			email: ['', [Validators.required, Validators.email] ],
			password: ['', [Validators.required]]
		})

		this.formLearner = this.$fb.group({
			codeAccess: ['', [Validators.required]],
		})

		this.innerWidth = window.innerWidth;


		window.addEventListener('resize', ( event: any ) => {
			this.innerWidth = event.target.innerWidth - 40;
		})

	}


	sendFormAdmin({ value }: { value: {email: string, password: string } }): void {
		this.$authSrv
			.signinAdmin( value.email, value.password )
			.subscribe(
				res => this.$router.navigate( ['/admin'] ),
				err => this.$snackBar.open(err.message, null, { duration: 3000 })
			)
	}


	sendFormLearner ({ value }): void {
		this.$authSrv.signinLearner(value.codeAccess)
			.subscribe(
				res => this.$router.navigate(['/learner']),
				err => this.$snackBar.open(err.message, null, { duration: 3000 })
			)
	}


	changeAdminSignin (): void {
		this.isAdminSignin = true
	}


	changeLearnerSignin (): void {
		this.isAdminSignin = false
	}
}