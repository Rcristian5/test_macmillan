import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterState, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth'
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {


	constructor(
		private $router: Router,
		private $authSrv: AuthService
	) {}


	canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> {
		return this.$authSrv.isLoggedIn
			.pipe(
				map( isLogged => {

					if ( isLogged.isLogin ) {

						if (isLogged.role === 'admin') {
							if (state.url.search('/manager') === -1) {
								this.$router.navigate(['/manager'])
								return false
							}
							return true
						}

						if (isLogged.role === 'teacher') {
							if (state.url.search('/teacher') === -1) {
								this.$router.navigate(['/teacher'])
								return false
							}
							return true
						}

						if (isLogged.role === 'learner') {
							if (state.url.search('/learner') === -1) {
								this.$router.navigate(['/learner'])
								return false
							}
							return true
						}

						return false
					}

					if( state.url !== '/login' ) {
						this.$router.navigate(['/login'])
						return false
					}
					return true

				}),
				catchError(() => {
					this.$router.navigate(['/login']);
					return of(false);
				})
			)
	}

}