import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { SigninComponent } from './components/signin/signin'
import { AuthGuard } from './guards/auth'


const routes: Routes = [
	{
		path: '',
		children: [],
		canActivate: [ AuthGuard ]
	},
	{
		path: 'login',
		component: SigninComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'manager',
		canActivate: [AuthGuard],
		children: [
			{
				path: 'dashboard',
				loadChildren: '../common/dashboard/dashboard.config#DashboardModule',
			},
			{
				path: 'block',
				loadChildren: '../admin/block/block.config#BlockModule',
			},
			{
				path: 'school',
				loadChildren: '../admin/school/school.config#SchoolModule',
			},
			{
				path: 'tests',
				loadChildren: '../admin/test/test.config#TestModule',
			},
			{
				path: '**',
				redirectTo: '/manager/dashboard'
			}
		]
	},
	{
		path: 'teacher',
		canActivate: [AuthGuard],
		children: [
			{
				path: 'dashboard',
				loadChildren: '../common/dashboard/dashboard.config#DashboardModule',
			},
			{
				path: 'profile',
				loadChildren: '../teacher/profile/profile.config#ProfileModule',
			},
			{
				path: 'tests',
				loadChildren: '../teacher/test/test.config#TestModule',
			},
			{
				path: 'groups',
				loadChildren: '../teacher/groups/groups.config#GroupsModule',
			},
			{
				path: '**',
				redirectTo: '/teacher/dashboard'
			}
		]
	},
	{
		path: 'learner',
		canActivate: [AuthGuard],
		loadChildren: '../learner/learner.config#LearnerModule',
	},
	{
		path: '**',
		children: [],
		canActivate: [AuthGuard]
	}
]

@NgModule({
	imports: [
		RouterModule.forRoot( routes ),
	],
	providers: [
		AuthGuard
	],
	exports: [
		RouterModule,
	]
})

export class WebappRoutingModule { }